const {
    SingleThing,
    WebThingServer,
    MultipleThings
} = require('webthing')
const { MyStrom } = require('./things/my-strom')
const { DS18B20Sensor } = require('./things/ds18b20')
//const { Buzzer } = require('./things/buzzer')
const { DHT22Sensor } = require('./things/dht22')
const { Led } = require('./things/LED')
const log = require('./things/print')
// Function that runs the server
function runServer() {
    const port = process.argv[2] ? Number(process.argv[2]) : 3003;

	// read active things from db
	//for each of them
		// create new instance
		// add it to multipleThings
	// start server with multiple things

    // Creating my new "Things"
    const myStrom1 = new MyStrom({ baseUrl: 'http://192.168.2.152/', name: 'Kitchen Lamp' })
    const myStrom2 = new MyStrom({ baseUrl: 'http://192.168.2.153/', name: 'Balcony Lights' })
    const tempSensor = new DS18B20Sensor()
    //const myBuzzer = new Buzzer()
    const tempHumSensor = new DHT22Sensor()
    const newLED = new Led()
    // Creating multiple "Things" and adding on the server
    const multipleThings = new MultipleThings([myStrom1, myStrom2, tempSensor, tempHumSensor, newLED], 'MultipleThings')
    const server = new WebThingServer(multipleThings, port);
    // Stop the Server
    process.on('SIGINT', () => {
        log('\n\n\n SIGINT received. wts exiting....');
        server.stop();
        process.exit();
    });
    process.on('uncaughtException', function (err) {
        log('Caught exception: ' + err);
    });
    // Starting the server
    server.start();
    log('WTS started...')

}

runServer()
