const { Led } = require('../LED')

describe('Led', () => {
	var led
	console.log(led)
	beforeAll(() => {
		led = new Led()
	})
	
	it('tests a test', () => {
		var LED = jasmine.createSpyObj('LED', ['writeSync'])
		led.initLed = () => {
			return LED
		}
		let value = 0
		led.onOff(value)
		expect(LED.writeSync).toHaveBeenCalledWith(value)
	})
})
