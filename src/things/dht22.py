import Adafruit_DHT
import sys

# Sensor should be set to Adafruit_DHT.DHT11,
# Adafruit_DHT.DHT22, or Adafruit_DHT.AM2302.
sensor = Adafruit_DHT.DHT22
# sensor = Adafruit_DHT.AM2302

# Example using a Beaglebone Black with DHT sensor
# connected to pin P8_11.
# pin = 'P8_11'

# Example using a Raspberry Pi with DHT sensor
# connected to GPIO[pin no. sent as arg].
# For example for pin physical 18, BCM 24, we use 24   
pin = sys.argv[1] #24

# Try to grab a sensor reading.  Use the read_retry method which will retry up
# to 15 times to get a sensor reading (waiting 2 seconds between each retry).
humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)

# Note that sometimes you won't get a reading and
# the results will be null (because Linux can't
# guarantee the timing of calls to read the sensor).
# If this happens try again!
if humidity is not None and temperature is not None and humidity <= 100:
    # print('Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity))
    print('{"httpCode": "200", "type": "SUCCESS", "message": "All good.", "data": [{"type": "TEMPERATURE", "value": "'+str(temperature)+'"}, {"type": "HUMIDITY", "value": "'+str(humidity)+'"}]}')
else:
    print('{"httpCode": "400", "type": "ERROR", "message": "Something hit the fan in dht22.py.", "data": [{"type": "TEMPERATURE", "value": "'+str(temperature)+'"}, {"type": "HUMIDITY", "value": "'+str(humidity)+'"}]}')
