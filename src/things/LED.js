const {
    Action,
    Property,
    Thing,
    Value,
} = require('webthing')
const Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO
const log = require('./print')

class BlinkAction extends Action {
    constructor(thing, input) {
        log(input)
        super(43, thing, 'blink', input);
    }
    //The LED will blink 2 secounds
    performAction() {
        return new Promise((resolve) => {
            var value = 1
            var duration = 2
            var id = setInterval(() => {
                log('value: ' + value)
                this.thing.setProperty('on', value)
                value = 1 - value
                duration = duration - 0.2
                if (duration <= 0) {
                    log('done in setInterval')
                    this.thing.setProperty('on', 0)
                    clearInterval(id)
                    resolve()
                }
            }, 200)
        })
    }
}

class LedValue extends Value {
		   /**
   * Initialize the object.
   *
   * @param {*} initialValue The initial value
   * @param {function} valueForwarder The method that updates the actual value
   *                                  on the thing
   * 
   */
    constructor(initialValue, valueForwarder) {
        super(initialValue, valueForwarder)
	}
	initLed() {
		//create led gpio object
        return new Gpio(21, 'out')
	}
}
/**
 * A LED which stays On/Off.
 */
class Led extends Thing {
    constructor() {
        super('LED', 'OnOffSwitch', 'A web connected LED')
        // On/Off property
        this.addProperty(
            new Property(
                this,
                'on',
                new LedValue(false, this.onOff),
                {
                    '@type': 'OnOffProperty',
                    label: 'On/Off',
                    type: 'boolean',
                    description: 'Whether the LED state is changed',
                }
            )
        )
        // Blinking action
        this.addAvailableAction(
            'blink',
            {
                label: 'blink',
                description: 'Blink for short time'
            },
            BlinkAction)
    }

    // Turn LED On/Off
    onOff(value) {
        log('callling LED onOff: ' + value)
        value = Number(value)
		let gpioObj = this.initLed()
        gpioObj.writeSync(value)
    }
}

module.exports = { Led }
