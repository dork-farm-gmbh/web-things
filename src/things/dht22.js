const {
  Property,
    Thing,
    Value,
} = require('webthing')
const ds18b20 = require('ds18b20-raspi')
const { PythonShell } = require('python-shell')
const log = require('./print')

/**
 * A temperature sensor which updates its measurement every few minutes.
 */
class DHT22Sensor extends Thing {
    constructor() {
        super('Temperature and Humidity', [],
            'A dht22 web connected temperature and humidity sensor')
        // Creating the temperature property
        this.temperature = new Value(42.0)
        this.addProperty(
            new Property(
                this,
                'temperature',
                this.temperature,
                {
                    '@type': 'LevelProperty',
                    label: 'Temperature',
                    type: 'number',
                    description: 'The current temperature in Celsius',
                    minimum: -10,
                    maximum: 50,
                    unit: 'celsius',
                }))
        // Creating the humidity property
        this.humidity = new Value(33.0)
        this.addProperty(
            new Property(
                this,
                'humidity',
                this.humidity,
                {
                    '@type': 'LevelProperty',
                    label: 'Humidity',
                    type: 'number',
                    description: 'The current humidity in %',
                    minimum: 0,
                    maximum: 100,
                    unit: 'percent',
                }))

        // Poll the sensor reading every 5 min
        setInterval(() => {
            PythonShell.run('dht22.py', { scriptPath: '/home/pi/webthing/things/', args: [24] }, (err, results) => {
                results = JSON.parse(results)
                results.data.forEach(entry => {
                    if (entry.type == "TEMPERATURE") {
                        this.temperature.notifyOfExternalUpdate(Number(entry.value).toFixed(2))
                        log('new temp level: '+ Number(entry.value).toFixed(2))

                    } else {
                        this.humidity.notifyOfExternalUpdate(Number(entry.value).toFixed(2))
                        log('My humidity: ' + Number(entry.value).toFixed(2))
                    }
                })
                // Update the underlying value, which in turn notifies all listeners
            })
        }, 300000)
    }
}
module.exports = { DHT22Sensor }