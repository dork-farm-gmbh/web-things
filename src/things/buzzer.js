const {
    Action,
    Property,
    Thing,
    Value,
} = require('webthing')
const { PythonShell } = require('python-shell')
const log = require('./print')
const Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO

class BuzzAction extends Action {
    constructor(thing, input) {
        log(input)
        super(42, thing, 'buzz', input);
    }
    // This action triggers the buzzer for 2 seconds
    performAction() {
        return new Promise((resolve) => {
            var value = 1
            var duration = 2
            var id = setInterval(() => {
                log('value: ' + value)
                this.thing.setProperty('on', value)
                value = 1 - value
                duration = duration - 0.2
                if (duration <= 0) {
                    log('done in setInterval')
                    //Calling multiple times off to be sure.
                    this.thing.setProperty('on', 0)
                    this.thing.setProperty('on', 0)
                    this.thing.setProperty('on', 0)
                    this.thing.setProperty('on', 0)
                    clearInterval(id)
                    resolve()
                }
            }, 200)
        })
    }
}
/**
 * A buzzer which buzz for 2 secounds.
 */

class Buzzer extends Thing {
    constructor() {
        super('Buzzer', 'OnOffSwitch', 'A web connected Buzzer')
        // Creating an On/Off property
        this.addProperty(
            new Property(
                this,
                'on',
                new Value(false, this.onOff),
                {
                    '@type': 'OnOffProperty',
                    label: 'On/Off',
                    type: 'boolean',
                    description: 'Whether the buzzer state is changed',
                }
            )
        )
        // Trigger the buzzer for 2 secounds
        this.addAvailableAction(
            'buzz',
            {
                label: 'buzz',
                description: 'Buzz short time'
            },
            BuzzAction);
    }
   /*
    initLed() {
		//create led gpio object
        return gpioObj = new Gpio(25, 'out'); //use GPIO pin 24, and specify that it is output
	}
	*/
	// Turn buzzer On/Off
    onOff(value) {
        log('callling Buzzer onOff: ' + value)
        value = Number(value)
        //let gpioObj = this.initLed()
        var gpioObj = new Gpio(25, 'out')
        gpioObj.writeSync(value)
    }
}

module.exports = { Buzzer }

