const request = require('request')

describe('Server', () => {
	var server
	beforeAll(() => {
		//server = require('../index')
	})
	afterAll(() => {
		//server.close()
	})
	describe("GET /", () => {
		var data = {}
		
		beforeAll( done => {
			request('http://localhost:3003', (err, res, body) => {
				console.log('status: ', res.statusCode)
				data.status = res.statusCode
				data.body = body
				done()
			})
		})
		
		it('Status 200', () => {
			expect(data.status).toBe(200)
		})
		
		it('Body to be string', () => {
			expect(typeof data.body).toBe("string")
		})

		it('Body to be object, having a name and href defined and as string, JSON parsing', () => {			
			let body = JSON.parse(data.body)
			expect(typeof body).toBe("object")
			body.forEach(entry => {
				expect(entry.name).toBeDefined()
				expect(typeof entry.name).toBe("string")
				expect(entry.href).toBeDefined()
				expect(typeof entry.href).toBe("string")
			})
		})
	})
})
