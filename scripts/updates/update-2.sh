#! /bin/bash
. /etc/environment
. "$WEB_THINGS_ROOT/scripts/localConfig.sh"

echo "************************************"
echo "**********Installing PM2************"
echo "************************************"
npm install pm2 -g
npm install jasmine -g

sed -i '/PM2_HOME/d' /etc/environment
echo "PM2_HOME=/home/$WEB_THINGS_USER_NAME/.pm2/" >> /etc/environment


echo "***************************"
echo "***save current version****"
echo "***************************"
echo "VERSION_CURRENT=2" > $WEB_THINGS_ROOT/scripts/patch.version

echo "************************************"
echo "***shuting down. please restart and run: cd /webThings/scripts && sudo ./initialSetup.sh***************"
echo "************************************"
shutdown now
