#! /bin/bash

apt -y install curl build-essential g++

. /etc/environment


echo "************************************"
echo "*************Setup PM2**************"
echo "************************************"

sudo su $WEB_THINGS_USER_NAME -c 'pm2 list'
chmod -R go+w "/home/$WEB_THINGS_USER_NAME/.pm2/"
sudo su -c "env PATH=$PATH:/usr/bin/node pm2 startup systemd -u $WEB_THINGS_USER_NAME --hp /home/$WEB_THINGS_USER_NAME"
sudo su -c "env PATH=$PATH:/usr/bin/node pm2 logrotate -u $WEB_THINGS_USER_NAME"
cd /webThings
sudo su $WEB_THINGS_USER_NAME -c "pm2 start $WEB_THINGS_ROOT/pm2-config.json"
sudo su $WEB_THINGS_USER_NAME -c 'pm2 save'

. /etc/environment;
sed -i '/NODE_PATH/d' /etc/environment
sed -i '/WEB_THINGS_ENV/d' /etc/environment

sudo su $WEB_THINGS_USER_NAME -c 'pm2 kill'
sudo su $WEB_THINGS_USER_NAME -c "pm2 start $WEB_THINGS_ROOT/pm2-config.json"
sudo su $WEB_THINGS_USER_NAME -c 'pm2 save'

# apt-get install python3 python3-pip -y
