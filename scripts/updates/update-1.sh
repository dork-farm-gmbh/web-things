#! /bin/bash
# This will turn off "frontend" (prompts) during installations
export DEBIAN_FRONTEND=noninteractive

locale-gen en_GB.UTF-8

. /etc/environment
. "$WEB_THINGS_ROOT/scripts/localConfig.sh"

echo "************************************"
echo "**********Getting updates***********"
echo "************************************"
apt -qq update
apt -qq upgrade


echo "*************************"
echo "**********Install node 8.x***********"
echo "*************************"
wget -qO- https://deb.nodesource.com/setup_8.x | sudo bash -
apt-get -y install nodejs

echo "*************************"
echo "**********Done***********"
echo "*************************"
