#! /bin/bash

allowed_envs="dev devpi production"
if [[ -z ${1+x} || ! $allowed_envs =~ $1 ]]
    then echo -e "\n Please provide environment: dev | devpi | production \n"; exit 2
fi

echo -e "\n"
echo "Setting WEB_THINGS_ENV to $1"
echo -e "\n"
if hash pm2 2>/dev/null; then
    pm2 stop pm2-config.json
    pm2 delete pm2-config.json
    cp pm2-config.json_tpl pm2-config.json
    sed -i "s/\"DEV\"/\"$1\"/g" pm2-config.json
    pm2 start pm2-config.json
    pm2 save
else
    echo "pm2 not installed yet"
fi
