#! /bin/bash
if [[ "$EUID" -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   echo "please run: sudo $(readlink -f $0) or sudo !!"
   exit 1
fi

. /etc/environment
. "$WEB_THINGS_ROOT/scripts/localConfig.sh"

if getent passwd $WEB_THINGS_USER_NAME; then
    echo "User $WEB_THINGS_USER_NAME exists, skipping."
else
    useradd $WEB_THINGS_USER_NAME -s /bin/bash -m
    echo "$WEB_THINGS_USER_NAME:$WEB_THINGS_USER_PASS" | chpasswd

    usermod -aG sudo $WEB_THINGS_USER_NAME
    # this adds the current running user to WEB_THINGS_USER_NAME's group; might be helpful in using PM2
    usermod -aG $WEB_THINGS_USER_NAME `logname`
    chown -R $WEB_THINGS_USER_NAME:$WEB_THINGS_USER_NAME $WEB_THINGS_ROOT

    # add WEB_THINGS_USER_NAME to spi group
    egrep -i "^spi" /etc/group;
    if [ $? -eq 0 ]; then
        usermod -a -G spi $WEB_THINGS_USER_NAME
    fi

    # add WEB_THINGS_USER_NAME to gpio group
    egrep -i "^gpio" /etc/group;
    if [ $? -eq 0 ]; then
        usermod -a -G gpio $WEB_THINGS_USER_NAME
    fi

    sed -i '/WEB_THINGS_USER_NAME/d' /etc/environment
    echo "WEB_THINGS_USER_NAME=$WEB_THINGS_USER_NAME" >> /etc/environment
    . /etc/environment
    echo "created user $WEB_THINGS_USER_NAME"
fi
